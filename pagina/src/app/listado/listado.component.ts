import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  ciudades:Array<object>;
  constructor() {
    this.ciudades = ["Medellin", "Bogotá", "Cali"];
  }

  ngOnInit(): void {
  }

}
